FROM python:3.10
WORKDIR /code
RUN adduser builder
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY . .
RUN chown -R builder /code
RUN chmod +x ./start.sh
USER builder
CMD ["./start.sh"]
